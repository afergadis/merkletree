#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "MD5.h"

typedef struct tree_node *Treeptr;

typedef struct tree_node {
    unsigned int digest[4];
    Treeptr *nodes;
    unsigned short int totalNodes;
    unsigned short int numOfNodes;
    short int level;
} Treenode;

Treeptr create_node(int level, int totalNodes);
Treeptr add(Treeptr p, unsigned char *h);
void print(Treeptr p, int indent);
void free_tree(Treeptr p);
void tree_cmp(Treeptr f1, Treeptr f2);

void usage(char *filename) {
    printf("usage: %s FILE1 FILE2 PAGE_SIZE TREE_FAN_OUT\n", filename);
    printf("\t%-12s: %s\n", "PAGE_SIZE", "the number of bytes for a page.");
    printf("\t%-12s: %s\n", "TREE_FAN_OUT", "the number of pages per tree node.");
}

int main(int argc, char** argv) {
    // Check input parameters (usage)
    unsigned int page_size;
    unsigned short fan_out;
    if (argc != 5) {
        usage(argv[0]);
        return EXIT_FAILURE;
    } else {
        if ((page_size = atoi(argv[3])) < 1) {
            printf("Invalid argument \"%s\" for PAGE_SIZE. Please use a positive integer.\n", argv[3]);
            usage(argv[0]);
            return EXIT_FAILURE;
        }
        if ((fan_out = atoi(argv[4])) < 2) {
            printf("Invalid argument \"%s\" for TREE_FAN_OUT. ", argv[4]);
            printf("Please use a positive integer greater or equal to 2.\n");
            usage(argv[0]);
            return EXIT_FAILURE;
        }
    }

    /* Create the first node of the trees */
    Treeptr f1_tree = create_node(1, fan_out);
    Treeptr f2_tree = create_node(1, fan_out);
    Treeptr f1_root = f1_tree; /* Keep a pointer to the current root */
    Treeptr f2_root = f2_tree;

    FILE *fp;
    size_t read;
    char *buffer = malloc(page_size * sizeof (char));

    // Open FILE1 and create a tree for it
    fp = fopen(argv[1], "rb"); // Open the file for binary input
    if (fp == NULL) {
        perror("Could not open file");
        return 1;
    }

    do {
        read = fread(buffer, 1, page_size, fp);
        if (read > 0) // we read a page
            if (read < page_size) // fewer bytes than requested
                memset(buffer + read, 0, page_size - read); // fill with zeros
        if ((f1_tree = add(f1_root, buffer)) != NULL) // add page to the tree
            f1_root = f1_tree;
    } while (read == page_size); // end when a read returned fewer bytes
    if (ferror(fp)) {
        perror("An error occured while reading");
        return EXIT_FAILURE;
    }
    fclose(fp);

    // Open FILE1 and create a tree for it
    fp = fopen(argv[2], "rb"); // Open the file for binary input
    if (fp == NULL) {
        perror("Could not open file");
        return 1;
    }

    do {
        read = fread(buffer, 1, page_size, fp);
        if (read > 0) // we read a page
            if (read < page_size) // fewer bytes than requested
                memset(buffer + read, 0, page_size - read); // fill with zeros
        if ((f2_tree = add(f2_root, buffer)) != NULL) // add page to the tree
            f2_root = f2_tree;
    } while (read == page_size); // end when a read returned fewer bytes
    if (ferror(fp)) {
        perror("An error occured while reading");
        return EXIT_FAILURE;
    }
    fclose(fp);

    printf("%s hash tree:\n", argv[1]);
    print(f1_root, 0);
    printf("%s hash tree:\n", argv[2]);
    print(f2_root, 0);
    tree_cmp(f1_root, f2_root);

    free_tree(f1_root);
    free(buffer);

    return (EXIT_SUCCESS);
}

/**
 * Calculates the digest of parameter data and puts the result on parameter digest.
 * 
 * @param data the input on witch we want the digest
 * @param digest the result of calculation
 * @return 0 if everything is ok, -1 if data or digest is NULL
 */
int MD5Digest(unsigned char *data, unsigned int *digest) {
    if (!digest || !data)
        return -1;

    MD5_CTX c;
    MD5Init(&c);
    MD5Update(&c, data, strlen(data));
    MD5Final(digest, &c);

    return 0;
}

/**
 * Calculates the digest for the concatenation of the child nodes for the input node.
 * Take the digest for every node's descendant and calculate the node digest.
 * Then, store the result in node's digest field.
 * 
 * @param r the node on witch we want the calculation
 */
void MD5ConDigest(Treeptr r) {
    // Υπολογισμός της σύνοψης της παράθεσης των συνόψεων των κόμβων παιδιών
    unsigned short i;
    unsigned int concatenation_digest[4];
    MD5_CTX c;
    MD5Init(&c);
    for (i = 0; i < r->numOfNodes; i++)
        MD5Update(&c, (char*) r->nodes[i]->digest, 4 * sizeof (int));
    MD5Final(concatenation_digest, &c);
    memcpy(r->digest, concatenation_digest, 4 * sizeof (int));
}

/**
 * Creates a new node with the give level and total number of child nodes.
 * 
 * @param l the level of the node
 * @param t the total number of subnodes the node has
 * @return a pointer to the new node
 */
Treeptr create_node(int l, int t) {
    Treeptr p = malloc(sizeof (Treenode));
    p->level = l;
    p->totalNodes = t;
    p->numOfNodes = 0;
    p->nodes = malloc(sizeof (Treeptr) * t);

    return p;
}

/**
 * Creates a new node as parent for the node in the parameter.
 * The child node is added to the parent's childs.
 * 
 * @param c the child node
 * @return a pointer to the parent node
 */
Treeptr add_parent(Treeptr c) {
    Treeptr r = create_node(c->level + 1, c->totalNodes);
    r->nodes[r->numOfNodes++] = c;

    MD5ConDigest(r);

    return r;
}

/**
 * Creates a child node for the node in the parameter and adds it to the parent node.
 * 
 * @param r the parent node
 * @return a pointer to the newly created child node
 */
Treeptr add_child(Treeptr r) {
    Treeptr child = create_node(r->level - 1, r->totalNodes);
    r->nodes[r->numOfNodes++] = child;

    return r->nodes[r->numOfNodes - 1];
}

/**
 * Search for a node that has less children than the total number it can.
 * 
 * @param r a pointer to the node to examine
 * @return
 *      - NULL if the node and all of it's children are complete
 *      - a pointer to the node that hasn't all it's children yet
 */
Treeptr search(Treeptr r) {
    unsigned short i;

    /* If r is of level 1, check only if it has all it's child.
     * We can't go any lower than this.
     */
    if (r->level == 1) {
        if (r->numOfNodes == r->totalNodes)
            return NULL; // Completed node
        else
            return r; // Not completed node
    } else if (r->level > 1) { // We have to search first it's children
        for (i = 0; i < r->numOfNodes; i++) { // For all it's children it has
            Treeptr result = search(r->nodes[i]);
            if (result != NULL) // Did we found a sub node that is not completed yet?
                return result; // return the sub node
        }
    }

    // We have examined all children of r, but not r yet
    if (r->numOfNodes < r->totalNodes) // Is the current node completed?
        return r; // return the current node

    return NULL; // The current node and all of it's children are complete
}

Treeptr add(Treeptr r, unsigned char *data) {
    // In case we have to add a parent in order to store data, we have to have
    // a pointer to the parent witch will be the new root of the tree.
    Treeptr root = NULL; // We haven't change the root
    Treeptr s = search(r); // s has the node on witch we can add data, or null if we can't
    if (s != NULL) { // There is a node to store data
        if (s->level == 1) { // The node is above leafs
            Treeptr leaf = create_node(0, r->totalNodes); // Create a new leaf node
            // Calculate the data digest and store it to the leaf->digest
            unsigned short md5_result = MD5Digest(data, leaf->digest);
            if (md5_result != 0) { // Something is wrong!
                free(leaf);
                printf("Error while calculating digest for input: %s\n", data);
                exit(1);
            }
            s->nodes[s->numOfNodes++] = leaf; // Add the node as a child
        } else { // The node isn't on level 1 and it's not complete
            // We have to add a child node on this node (s)
            Treeptr c = add_child(s);
            // Try to add data on child node
            add(c, data);
        }
        // Calculate the concatenated digest of the node on witch we added a leaf or a child node
        MD5ConDigest(s);
        // Calculate the digest for the current node (r) because we had an insertion in one of it's childer
        MD5ConDigest(r);
    } else { // Node r and all of it's children are completed
        // We have to create a new root as parent of the current (r)
        root = add_parent(r); // Keep a pointer to the new root
        add(root, data); // Try to add the data to the new node now
    }

    return root; // If the root hasn't changed, return NULL, otherwise the pointer to the new root.
}

/**
 * Prints the contents of a tree
 * 
 * @param p the node to print
 * @param indent a number of characters to use indentation 
 */
void print(Treeptr p, int indent) {
    static unsigned int page = 1;
    unsigned short i, k;
    if (p != NULL) {
        for (i = 0; i < p->numOfNodes / 2; i++) // For the 1st half of children
            print(p->nodes[i], indent + 4); // For the subnode add 4 characters to the indentation
        for (k = 0; k < indent; k++) // Print indent number of spaces
            printf(" ");
        MD5Print(p->digest);
        printf("\n");
        for (i = p->numOfNodes / 2; i < p->numOfNodes; i++) // For the 2nd half of children
            print(p->nodes[i], indent + 4);
    }
}

/**
 * Frees the memory of a tree node
 * 
 * @param r the node to free
 */
void free_tree(Treeptr r) {
    unsigned short i;
    // First free all it's children nodes
    for (i = 0; i < r->numOfNodes; i++)
        free_tree(r->nodes[i]);

    // The nodes has freed all it's children, so free the node.
    free(r);
}

/**
 * Compares two digests
 * 
 * @param f the 1st digest to compare
 * @param s the 2nd digest to compare
 * @return 
 *      - 0 if equal
 *      - 1 if now equal
 */
int digests_cmp(unsigned int *f, unsigned int *s) {
    unsigned short i;
    for (i = 0; i < 4; i++)
        if (f[i] != s[i])
            return 1;

    return 0;
}

/**
 * Compare two trees and prints the pages where they are different (if any).
 * 
 * @param f1 a pointer to the 1st tree node
 * @param f2 a pointer to the 2st tree node
 */
void tree_cmp(Treeptr f1, Treeptr f2) {
    static int page = 0;
    unsigned short i;
    
    if (f1->level != f2->level) {
        printf("The input files have different size.\n");
        return;
    }

    printf("Comparing ");
    MD5Print(f1->digest);
    printf(" with ");
    MD5Print(f2->digest);
    printf("\n");
    
    if (f1->level == 0 && f2->level == 0) { // We are on leaf level
        page++; // Increase the pages we have accessed
        if (digests_cmp(f1->digest, f2->digest) != 0)
            printf("The files are different on page: %d\n", page);
    } else {
        if (digests_cmp(f1->digest, f2->digest) == 0) { // Check the current nodes
            // The non leaf nodes are equal, so skip fan_out^(level+1) pages
            page += (int) pow((double) f1->totalNodes, (double) f1->level + 1);
            return;
        } else // Check the child nodes
            for (i = 0; i < f1->numOfNodes; i++)
                tree_cmp(f1->nodes[i], f2->nodes[i]);
    }
}